import React, { useState, useContext } from 'react';
import './NavAuth.scss';
import { useLogin } from './loginHook';
import UserContext from '../../../../data/context/UserContext';
import { SpinningCircle } from '../../../common/spinner';

export function NavAuth() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { state, dispatch } = useContext(UserContext);

  const { loginAction, loading, error } = useLogin();
  const handleLogin = (event) => {
    event.preventDefault();
    loginAction({ email, password });
  };

  const handleLogout = () => {
    //dispatched to Context (via reducer)
    dispatch({ type: 'LOGOUT' });
  };

  return (
    <div className='nav-auth-wrapper'>
      <div className='nav-auth-form'>
        {state.isAuthenticated ? (
          <button onClick={handleLogout}>logout</button>
        ) : (
          <form onSubmit={handleLogin}>
            {loading ? <SpinningCircle /> : <input type='submit' value='login' />}
            {error && <span>...something went wrong</span>}

            <input
              placeholder='email'
              type='text'
              onChange={(event) => setEmail(event.target.value)}
              value={email}
            />
            <input
              placeholder='password'
              type='password'
              onChange={(event) => setPassword(event.target.value)}
              value={password}
            />
          </form>
        )}
      </div>
    </div>
  );
}
