import { useState, useEffect, useContext } from 'react';
import { post } from '../../../../data/api';
import { setTokenValue } from '../../../../data/auth';
import UserContext from '../../../../data/context/UserContext';

const initialCredentials = { email: '', password: '' };

export const useLogin = () => {
  // input login credentials
  const [credentials, setCredentials] = useState(initialCredentials);
  // loading passed to component hooked to useLogin
  const [loading, setLoading] = useState(false);
  //result of login (if needed)
  const [result, setResult] = useState(null);
  //error message and status.
  const [error, setError] = useState(null);

  const { dispatch } = useContext(UserContext);

  const loginAction = ({ email, password }) => {
    setCredentials({ email, password });
  };

  useEffect(() => {
    if (credentials.email && credentials.password) {
      setLoading(true);
      post('/auth/login', credentials)
        .then((res) => {
          dispatch({ type: 'LOGIN', payload: res });
          setResult(res);
          setTokenValue(res.token);
        })
        .catch((err) => setError(err));
      setLoading(false);
      setCredentials(initialCredentials);
    }
  }, [credentials, dispatch]);

  return { loginAction, result, loading, error };
};
